/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Triangle.h"

Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}

Triangle::~Triangle() {

}

void Triangle::setA(double a) {
	this->a = a;  //this anahtar kelimesi kullan�ld�
}

void Triangle::setB(double b) {  //parametresi d�zeltildi
	this->b = b;       //this kullan�ld� 
}

void Triangle::setC(double c) {   //parametresi d�zeltildi
	this->c = c;   // Bu sat�r d�zeltildi
}

double Triangle::calculateCircumference() {
	return (a + b + c);   //D�zeltildi
}

