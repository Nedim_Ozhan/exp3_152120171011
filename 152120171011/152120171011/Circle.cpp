/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
#include <iostream>
using namespace std;
//namespace Student1 { //namespace i yorum satırına aldık
	Circle::Circle(double r) {
		setR(r);
	}

	Circle::~Circle() {
	}

	void Circle::setR(double r) {
		this->r = r;
	}

	double Circle::getR()const { // Const anahtar kelimesi ile kısıtlandırıldı
		return r;
	}

	double Circle::calculateCircumference()const { // Const anahtar kelimesi ile kısıtlandırıldı
		return PI * r * 2;  //Düzeltildi
	}

	double Circle::calculateArea()const { // Const anahtar kelimesi ile kısıtlandırıldı
		return PI * r * r;  //Düzeltildi
	}
	bool Circle::is_Equal(Circle* c1, Circle* c2)const {
		if (c1->getR() == c2->getR()) {
			cout << "Equals Circles !";
			return true;
		}
		cout << "Not Equals Circles !";
		return false;
	}
	void Circle::setR(int r) {
		this->r = r;
	}
//}
