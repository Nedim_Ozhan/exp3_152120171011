/*
 * Square.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef SQUARE_H_
#define SQUARE_H_
#define PI 3,14   //PI 3,14 olarak tanımlandı
class Square {
public:
	Square(double);
	virtual ~Square();
	void setA(double);
	void setB(double);  
	double calculateCircumference();
	double calculateArea();
private:
	double a;
	double b;  
};

#endif /* SQUARE_H_ */

