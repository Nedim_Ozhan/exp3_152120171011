/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
#define PI 3,14   //PI 3,14 olarak tanımlandı
class Circle {
public:
	Circle(double);
	virtual ~Circle();
	void setR(double);
	void setR(int);     //Overload 
	double getR()const;   //Nesne üyelerinde değişiklik yapılmaması için kısıtlandırıldı
	double calculateCircumference()const;  //Nesne üyelerinde değişiklik yapılmaması için kısıtlandırıldı
	double calculateArea()const; //Nesne üyelerinde değişiklik yapılmaması için kısıtlandırıldı
	bool is_Equal(Circle* c1, Circle* c2)const;
private:
	double r;
	//double PI = 3.14; //PI yi define ettiğimizden bu satırı yorum olarak aldık
};
#endif /* CIRCLE_H_ */

